# Web application example in vanilla javascipt

This is an attempt to build a very simple, but working application in plain javascipt. JS community has an incredible pace, and new frameworks pop out every other day – but more and more people become more and more far from understanding basic principles of JS applications, which are not rocket science at all. It is ease to come across a newbie who is trying to figure out what the hell webpack is, and is really need all this isomorphic stuff with relay server around sample react application.

So, I've tried to create very-very simple application with detailed explanation of every step. It is not a framework, but just an application which can be written to fetch some data by search query typed in input and to display it. This is quite a common task (especially in the interviews) – and very often people asked how they would implement this simple task start to depend on big projects just to get 1% of their functionality, write a lot of boilerplate and end up with a spaceship to visit next village. I personally think that the problem is that we dive into frameworks, and never discuss actual implementation principles (not the intrinsic details, though). It is considered, that "true way" can be introduced only by cool developers with a lot of subscribers, but actually what we need – understanding how it works, and how we can apply it for our tasks. So, if you need to write something custom, and it hardly fits into existing solution, don't be shy to refuse this idea just because you are not cool enough – sometimes requirements are really special.

So, if you want to take a look, just read the `src` folder, starting with `index.js` and then `main.js`.

## Reasoning

commit 08ce673a7d70083f90152c424979ab0264f4f4b9 (HEAD -> dev, origin/dev, origin/branch1, branch1)
Merge: 2150d62 91281a3
Author: Karthick <karthick@orchids.edu.in>
Date:   Tue Jul 21 08:05:56 2020 +0000

    Merge branch 'branch2' into 'dev'
    
    Branch2
    
    See merge request karthick9/git-assingment!2

commit 91281a3b0a26067664e66afd4b0311229cb30cc4 (origin/branch2, branch2)
Merge: 69f6ba9 5191e76
Author: Karthick <karthick@orchids.edu.in>
Date:   Tue Jul 21 13:34:45 2020 +0530

    merge branch1

commit 69f6ba93bf7e5fea9f2ae4b3709a90cccaa12ced
Author: Karthick <karthick@orchids.edu.in>
Date:   Tue Jul 21 13:33:05 2020 +0530

    added second page href

commit 5191e76bcf153ca844955dfc5cc0c7c751eb8b49
Author: Karthick <karthick@orchids.edu.in>
Date:   Tue Jul 21 13:29:15 2020 +0530

    added second page

commit 2150d626815e975815eb2d7a37a9db27abf9296a
Merge: d72ec29 a8f922c
Author: Karthick <karthick@orchids.edu.in>
Date:   Tue Jul 21 07:53:21 2020 +0000

    Merge branch 'branch2' into 'dev'
    
    Branch2
    
    See merge request karthick9/git-assingment!1

commit a8f922cc61fe33669c87dce958aff0d6c1485113
Merge: 5e3f92a 8816b5f
:

Why on earth creating another web application, especially in plain javascript? And when the reference application [exists](https://github.com/tastejs/todomvc/tree/gh-pages/examples/vanillajs) (which is quite obsolete to the moment – because modern applications without fetching data are not the most common case). I've written a [blog post](https://bloomca-me.github.io/2016/10/15/writing-web-application-in-plain-js.html) about it, but the general idea is to provide explanation how the basic application can be structured. This structure is not unique or special, and many frameworks have it in some way or another.
